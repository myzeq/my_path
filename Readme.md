# Foobar
my_path public project
## Installation
```bash
symfony new my_path --version=5.4 --webapp
semfony console make:user
symfony console make:migration
php bin/console doctrine:migrations:migrate

composer require "lexik/jwt-authentication-bundle" 

```

## Installation JWT Auth
https://github.com/lexik/LexikJWTAuthenticationBundle/blob/2.x/Resources/doc/index.md#getting-started

## Postman
https://www.getpostman.com/collections/a410b703179df6ae09b2